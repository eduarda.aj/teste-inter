import React from "react";
import Input from "./components/Input/Input";
import Button from "./components/Button/Button";
import ResultBox from "./components/ResultBox/ResultBox";

import { AuthContext } from "./providers/auth";

import "./Home.scss";

function Home() {
  const { pokemon } = React.useContext(AuthContext);

  return (
    <div className="home">
      <div className="home__container">
        <h1>Escolhe seu Pokemon</h1>
        <div className="home__wrapper">
          <Input />
          <Button />
        </div>
        {pokemon.data &&
          (!pokemon.data.isAxiosError ? (
            <ResultBox />
          ) : (
            <div>Não existe Pokemon com esse número, tente outro número.</div>
          ))}
      </div>
    </div>
  );
}

export default Home;
