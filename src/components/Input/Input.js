import React from "react";
import "./Input.scss";

import { AuthContext } from "../../providers/auth";

function Input() {
  const { setPokemon } = React.useContext(AuthContext);

  return (
    <div className="input-component">
      <div className="input-component__container">
        <input
          data-testid="input-pokemon"
          placeholder="Digite o número do Pokemon"
          type="number"
          onChange={(e) => setPokemon({ id: e.target.value, data: [] })}
        />
      </div>
    </div>
  );
}

export default Input;
