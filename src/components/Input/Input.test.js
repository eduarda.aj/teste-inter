import React from "react";
import { render, waitFor } from "@testing-library/react";

import Input from "./Input";

describe("Testes for input component", () => {
  it("Check if the input component is being rendered", async () => {
    const { getByTestId } = render(<Input />);
    const fieldNode = await waitFor(() => getByTestId("input-pokemon"));
  });
});
