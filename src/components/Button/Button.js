import React from "react";
import axios from "axios";
import "./Button.scss";

import { AuthContext } from "../../providers/auth";

function Button() {
  const { pokemon, setPokemon } = React.useContext(AuthContext);

  async function handleSearch(id) {
    if (Number(id)) {
      const pokemonResult = await axios
        .get(`${process.env.REACT_APP_URL}${id}`)
        .then((response) => response.data)
        .catch((error) => error);

      setPokemon({ id, data: pokemonResult });
    } else {
      setPokemon({ id, data: { name: "Favor digitar apenas números maiores que 1." } });
    }
  }

  return (
    <div className="button-component">
      <div className="button-component__container">
        <button data-testid="button-search" type="button" onClick={() => handleSearch(pokemon.id)}>
          Pesquisar
        </button>
      </div>
    </div>
  );
}

export default Button;
