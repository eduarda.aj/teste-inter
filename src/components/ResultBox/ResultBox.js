import React from "react";
import "./ResultBox.scss";

import { AuthContext } from "../../providers/auth";

function ResultBox() {
  const { pokemon } = React.useContext(AuthContext);

  return (
    <div className="home__pokemon pokemon">
      <p>{pokemon.data.name}</p>
      {pokemon.data.sprites && (
        <div className="pokemon__image">
          <img src={pokemon.data.sprites.back_default} alt={pokemon.data.name} />
          <img src={pokemon.data.sprites.back_shiny} alt={pokemon.data.name} />
          <img src={pokemon.data.sprites.front_default} alt={pokemon.data.name} />
          <img src={pokemon.data.sprites.front_shiny} alt={pokemon.data.name} />
        </div>
      )}
    </div>
  );
}

export default ResultBox;
