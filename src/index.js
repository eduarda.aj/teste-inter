import React from "react";
import ReactDOM from "react-dom";
import Home from "./Home";
import { AuthProvider } from "./providers/auth";

ReactDOM.render(
  <React.StrictMode>
    <AuthProvider>
      <Home />
    </AuthProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
