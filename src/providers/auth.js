import React, { useState } from "react";

export const AuthContext = React.createContext({});

export const AuthProvider = (props) => {
  const [pokemon, setPokemon] = useState({
    id: "",
    data: null,
  });

  return <AuthContext.Provider value={{ pokemon, setPokemon }}>{props.children}</AuthContext.Provider>;
};
