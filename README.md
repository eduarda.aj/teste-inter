# Teste Inter

Teste para vaga de frontend no Inter. Resumo: Inserir um número para buscar um Pokemon, com tratamento de erros, manipulação de dados, testes unitários e layout.

### 🔧 Instalação

- Clonar o projeto na sua máquina
- Instalar o yarn
- Para iniciar o projeto, digitar o comando: yarn start

## ⚙️ Executando os testes

Foram implementados testes de rendeziração dos componentes, usando a biblioteca Testing Library.
Para iniciar os testes:

- yarn test

## 📦 Melhorias

Futuramente seria interessante aplicar mais testes, além de transformar o css em Styled Component, para que todos os componentes tenha o css separado e mais enxuto.

## 🛠️ Construído com

Segue abaixo o que foi usado para construir esse projeto

- [React]
- [Axios]
- [TestingLibrary]
- [Eslint]
- [Prettier]

## ✒️ Autores

- **Camila Eduarda Amaral** - [eduarda.aj](https://gitlab.com/eduarda.aj)
